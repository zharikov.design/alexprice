$(document).ready(function(){

  // Fixed header
  $(window).scroll(function() {
    if ($(this).scrollTop() > 1){
      $('header').addClass('_fixed');
    }
    else{
      $('header').removeClass('_fixed');
    }
  });

  // Burger menu
  $('body').on('click', '#burger', function(){
    $(this).toggleClass('active');
    $('.header-nav-menu').toggleClass('active');
  });

  // First text
  $('.user #staffName').html(function () {
    var text = $(this).text().trim().split(' ');
    var first = text.shift();
    return (text.length >= 0 ? '<span>' + first + '</span>' : first) + text.join(' ');
  });

  // Slider reviews
  $('.reviews-company').slick({
    infinite: false,
    variableWidth: true,
    slidesToScroll: 1
  });

  // Tabs reviews
  $('.reviews-company').on('click', 'a:not(.active)', function() {
    $(this)
      .addClass('active').siblings().removeClass('active')
      .closest('.reviews').find('.reviews-content').removeClass('active').eq($(this).index()).addClass('active');
  });

  // Slider staff
  var t = $(window).width();
	if (t<960) {
		var full_weight=0;
    $('.staff-wrapp .scroll .staff-wrapp-item').each(function(){
      full_weight+=$(this).outerWidth(true);
      $('.staff-wrapp .scroll').css('width', full_weight);
      setTimeout(function() {
        $('.staff-wrapp .scroll').css('opacity', '1');
      }, 500);
    });
    $('.tablepress').wrap('<div class="scroll"></div>');
	}
	else {
  }
  
  // Capcha
  $('.wpcf7-submit').prop('disabled', true);
  $('body').on('click', '#js-capcha', function(){
    $(this).addClass('active');
    $('.contacts-wrapp-form_footer-button').removeClass('_disabled');
    $('.wpcf7-submit').prop('disabled', false);
  });
  $('body').on('click', '#js-capcha.active', function(){
    $(this).removeClass('active');
    $('.contacts-wrapp-form_footer-button').addClass('_disabled');
    $('.wpcf7-submit').prop('disabled', true);
  });

  // Tabs maps
  $('body').on('click', '#moscow-link', function(){
    $(this).addClass('active');
    $('#krasnodar-link').removeClass('active');
    $('#moscow').show();
    $('#krasnodar').hide();
  });
  $('body').on('click', '#krasnodar-link', function(){
    $(this).addClass('active');
    $('#moscow-link').removeClass('active');
    $('#moscow').hide();
    $('#krasnodar').show();
  });
});
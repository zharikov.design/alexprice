<?php get_header(); ?>
<article class="page-main" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
  	<?php endwhile; else: ?>
  <?php endif; ?>
</article>
<?php get_footer(); ?>
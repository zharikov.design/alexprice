<?php
/**
 * Template part for displaying posts
 *
 * @package alexprice
 */

?>

<div class="cover" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
<div class="date"><?php the_time('j F Y') ?></div>
<?php the_title( '<h1>', '</h1>' ); ?>
<?php the_content(); ?>

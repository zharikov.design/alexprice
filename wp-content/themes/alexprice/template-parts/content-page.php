<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alexprice
 */

?>

<article class="page-page" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<?php the_title( '<h1>', '</h1>' ); ?>
		<?php
			the_content();
		?>
	</div>
</article>

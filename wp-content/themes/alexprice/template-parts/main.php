<?php
/**
 * Template part for displaying posts
 *
 * @package alexprice
 */

?>
<a href="<?php the_permalink(); ?>" class="item">
	<div class="item-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
	<div class="item-content">
		<div class="item-content-title"><span><?php the_title(); ?></span></div>
	</div>
</a>


<?php
/**
 * Template Name: Услуга
 * Template Post Type: page
 */

get_header(); 
?>

<article class="page-page" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="cover">
		<div class="cover-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
		<div class="cover-title">
			<?php the_title( '<h1>', '</h1>' ); ?>

			<?php
				$field = get_field('individ',get_the_ID());
				if($field == '1'){
					echo "<span>Стоимость: ";
					the_field('individ-price');
					echo "</span>";
				}else{
					echo "<span>Стоимость: ";
					the_field('price');
					echo" руб.</span>";
				}
			?>

		</div>
	</div>	
	<div class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
  	<?php endwhile; else: ?>
    <?php endif; ?>
	</div>
</article>

<?php get_footer(); ?>
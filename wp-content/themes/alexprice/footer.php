<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alexprice
 */

?>

	<footer>
		<div class="footer-contacts">
			<div class="container">
				<div class="footer-contacts-wrapp">
					<div class="footer-contacts-wrapp_item">
						<div class="footer-contacts-wrapp_item-icon _phone"></div>
						<div class="footer-contacts-wrapp_item-text">(495) 150-6449</div>
						<div class="footer-contacts-wrapp_item-city">г. Москва</div>
					</div>
					<div class="footer-contacts-wrapp_item">
						<div class="footer-contacts-wrapp_item-icon _location"></div>
						<div class="footer-contacts-wrapp_item-text">Селезневская ул.,д.32, оф. 303</div>
						<div class="footer-contacts-wrapp_item-city">г. Москва</div>
					</div>
					<div class="footer-contacts-wrapp_item">
						<div class="footer-contacts-wrapp_item-icon _mail"></div>
						<div class="footer-contacts-wrapp_item-text">info@alexprice.ru</div>
						<div class="footer-contacts-wrapp_item-city">г. Москва</div>
					</div>
				</div>
				<div class="footer-contacts-wrapp">
					<div class="footer-contacts-wrapp_item">
						<div class="footer-contacts-wrapp_item-icon _phone"></div>
						<div class="footer-contacts-wrapp_item-text">(861) 258-4576</div>
						<div class="footer-contacts-wrapp_item-city">г. Краснодар</div>
					</div>
					<div class="footer-contacts-wrapp_item">
						<div class="footer-contacts-wrapp_item-icon _location"></div>
						<div class="footer-contacts-wrapp_item-text">Кореновская, 24</div>
						<div class="footer-contacts-wrapp_item-city">г. Краснодар</div>
					</div>
					<div class="footer-contacts-wrapp_item">
						<div class="footer-contacts-wrapp_item-icon _mail"></div>
						<div class="footer-contacts-wrapp_item-text">krasnodar@alexprice.ru</div>
						<div class="footer-contacts-wrapp_item-city">г. Краснодар</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
				<nav class="footer-copyright-nav">
					<?php wp_nav_menu(array('menu' => 'footer', 'menu_class' => 'footer-copyright-nav_menu', '' => '')); ?>
				</nav>
				<div class="footer-copyright-copy">Copyright © 2003 – <?php echo date('Y') ?> ООО НКЦ «Алекс-Прайс»</div>
				<a href="http://zharikov.design" class="footer-copyright-autor" target="_blank" title="Design by: Zharikov.design">
					<img src="<?php echo get_template_directory_uri() ?>/images/zharikov.png">
				</a>
			</div>
		</div> 
	</footer>


		<?php wp_footer(); ?>
	</body>
</html>

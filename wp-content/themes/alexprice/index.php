<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @package alexprice
 */

get_header();
?>
	<article class="page-news" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="main container">
	<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
					<h1><?php single_post_title(); ?> и события</h1>
					<h2>News</h2>
			
					<div class="main-wrapp">
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/main', get_post_type() );

			endwhile;

			

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</div>
		<?php the_posts_navigation(); ?>
	</div>
</article>

<?php get_footer(); ?>

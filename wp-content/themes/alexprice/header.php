<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alexprice
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/images/favicon//favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/images/favicon//favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri() ?>/images/favicon//site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/images/favicon//safari-pinned-tab.svg" color="#666666">
	<meta name="msapplication-TileColor" content="#64b9be">
	<meta name="theme-color" content="#64b9be">

	<link href='<?php echo get_template_directory_uri() ?>/style.css' media='screen, projection' rel='stylesheet' type='text/css'>
	<script src='<?php echo get_template_directory_uri() ?>/js/lib.min.js' type='text/javascript'></script>
  <script src='<?php echo get_template_directory_uri() ?>/js/scripts.min.js' type='text/javascript'></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header>
		<div class="header container">
      <a href="/" class="header-logo">
        <img src="<?php echo get_template_directory_uri() ?>/images/logo.svg">
      </a>
      <a href="/" class="header-name">Алекс Прайс</a>
      <nav class="header-nav">
        <a href="javascript:void(0);" class="header-burger" id="burger"></a>
        <?php wp_nav_menu(array('menu' => 'header', 'menu_class' => 'header-nav-menu', '' => '')); ?>
      </nav>
    </div>
  </header>
  <div class="header-fixed"></div>

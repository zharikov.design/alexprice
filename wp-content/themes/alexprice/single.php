<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package alexprice
 */

get_header();
?>

<article class="page-news" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="page container">
	<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>
	</div>
</article>

<?php
get_footer();
